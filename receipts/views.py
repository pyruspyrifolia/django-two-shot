from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptsForm, ExpenseCategoryForm, AccountForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def ListReceipts(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {
"receipt_list": receipt_list
    }

    return render (request, "receipts/ReceiptInfo.html", context)

@login_required
def CreateReceipt(request):
    if request.method == "POST":
        form = ReceiptsForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptsForm()

    context = {
        "form":form,
    }

    return render(request, "receipts/create.html", context)

@login_required
def category_list(request):
    things = ExpenseCategory.objects.filter(owner=request.user)
    context = {
    "category_list": things
    }

    return render (request, "receipts/ExpenseCategory.html", context)

@login_required
def account_list(request):
    things = Account.objects.filter(owner=request.user)
    context = {
    "account_list": things
    }

    return render (request, "receipts/Account_list.html", context)

@login_required
def CreateExpenseCategory(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            NewExpenseCategory = form.save(False)
            NewExpenseCategory.owner = request.user
            NewExpenseCategory.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    context = {
        "form":form,
    }

    return render(request, "receipts/createexpensecategory.html", context)

@login_required
def CreateAccount(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            NewAccount = form.save(False)
            NewAccount.owner = request.user
            NewAccount.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {
        "form":form,
    }

    return render(request, "receipts/createexpensecategory.html", context)
