from django.urls import path
from receipts.views import ListReceipts, CreateReceipt, category_list, account_list, CreateExpenseCategory, CreateAccount

urlpatterns = [
    path("", ListReceipts, name="home"),
    path("create/", CreateReceipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list" ),
    path("categories/create/", CreateExpenseCategory, name="create_category"),
    path("accounts/create/", CreateAccount, name="create_account")
]
